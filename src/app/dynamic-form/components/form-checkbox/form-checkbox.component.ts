import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
	selector: 'pt-form-checkbox',
	templateUrl: './form-checkbox.component.html',
	styleUrls: ['./form-checkbox.component.css']
})
export class FormCheckboxComponent implements OnInit {
	config;
	group: FormGroup;

	constructor() {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
	}

}
