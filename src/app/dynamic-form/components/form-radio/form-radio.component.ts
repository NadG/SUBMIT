import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {DynamicField} from '../dynamic-field.interface';

@Component({
	selector: 'pt-form-radio',
	templateUrl: './form-radio.component.html',
	styleUrls: ['./form-radio.component.css']
})
export class FormRadioComponent implements OnInit {
	config: DynamicField;
	group: FormGroup;

	constructor() {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
	}

}
