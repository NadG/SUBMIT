import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
	selector: 'pt-form-select',
	templateUrl: './form-select.component.html',
	styleUrls: ['./form-select.component.css']
})
export class FormSelectComponent implements OnInit {
	config;
	group: FormGroup;

	constructor() {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
	}

}
