export interface Validator {
	name: string;
	validator: any;
	message?: string;
}
export interface DynamicField {
	type: string;
	name: string;
	key: string;
	value?: any;
	label?: string;
	placeholder?: string;
	options?: string[];
	href?: string;
	target?: string;
	validations?: Validator[];
}
