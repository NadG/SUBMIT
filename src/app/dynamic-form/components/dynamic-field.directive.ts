import {ComponentFactoryResolver, Directive, Input, OnInit, ViewContainerRef} from '@angular/core';
//ViewContainerRef Represents a container where one or more Views can be attached.
import {FormGroup} from '@angular/forms';
import {FormInputComponent} from './form-input/form-input.component';
import {FormSelectComponent} from './form-select/form-select.component';
import {FormRadioComponent} from './form-radio/form-radio.component';
import {FormCheckboxComponent} from './form-checkbox/form-checkbox.component';
import {DynamicField} from './dynamic-field.interface';

const components = {
	input: FormInputComponent,
	select: FormSelectComponent,
	radio: FormRadioComponent,
	checkbox: FormCheckboxComponent
};

@Directive({
	selector: '[ptDynamicField]'
})
export class DynamicFieldDirective  implements OnInit{
	@Input() config: DynamicField;
	@Input() group: FormGroup;

	constructor(
		private resolver: ComponentFactoryResolver,
		private container: ViewContainerRef
	) {}

	ngOnInit() {
		let component = components[this.config.type];
		let factory = this.resolver.resolveComponentFactory<any>(component);
		component = this.container.createComponent(factory);
		component.instance.config = this.config;
		component.instance.group = this.group;


		// components[this.config.type] = ViewComponentRef.createComponent(
		//		 ComponentFactoryResolver.resolveComponentFactory<any>(input[this.config.type]
		// )
		// components[this.config.type].instance.config = this.config
		// components[this.config.type].instance.group = this.group
	}
}
