import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import { DynamicFieldDirective } from './components/dynamic-field.directive';
import { FormInputComponent } from './components/form-input/form-input.component';
import { FormSelectComponent } from './components/form-select/form-select.component';
import { DynamicFormComponent } from './container/dynamic-form/dynamic-form.component';
import { FormRadioComponent } from './components/form-radio/form-radio.component';
import { FormCheckboxComponent } from './components/form-checkbox/form-checkbox.component';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule
	],
	declarations: [
		DynamicFieldDirective,
		FormInputComponent,
		FormSelectComponent,
		DynamicFormComponent,
		FormRadioComponent,
		FormCheckboxComponent
	],
	entryComponents: [
		FormInputComponent,
		FormSelectComponent,
		FormRadioComponent,
		FormCheckboxComponent
	],
	exports: [
		DynamicFormComponent,
	],
})
export class DynamicFormModule {
}
//ANGULAR2: ERROR –
// NO COMPONENT FACTORY FOUND. DID YOU ADD IT TO @NGMODULE.ENTRYCOMPONENTS?
// Solution: Place components which are created dynamically to entryComponents under @NgModuledecorator function.
