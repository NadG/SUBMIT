import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DynamicField} from '../../components/dynamic-field.interface';

@Component({
	selector: 'app-dynamic-form',
	templateUrl: './dynamic-form.component.html',
	styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnInit {
	@Input() config: DynamicField[] = [];
	form: FormGroup;

	constructor(private fb: FormBuilder) {
	}

	ngOnInit() {
		console.log('this.constructor.name ' + this.constructor.name);
		this.form = this.createGroup();
	}

	get value() {
		return this.form.value;
	}

	createGroup() {
		const group = this.fb.group({});
		this.config.forEach(config => group.addControl(config.name,
			this.fb.control('',
				this.bindValidations(config.validations || [])
			)
		));
		return group;
	}

	bindValidations(validations: any) {
		if (validations.length > 0) {
			const validList = [];
			validations.forEach(valid => {
				validList.push(valid.validator);
			});
			return Validators.compose(validList);
		}
		return null;
	}

//	createControl() {
// const group = this.fb.group({});
// this.fields.forEach(field => {
// if (field.type === "button") return;
// const control = this.fb.control(
//  	field.value,
// 		this.bindValidations(field.validations || [])
// );
// group.addControl(field.name, control);
// });
// return group;
// }

//	bindValidations(validations: any) {
// if (validations.length > 0) {
// const validList = [];
// validations.forEach(valid => {
// validList.push(valid.validator);
// });
// return Validators.compose(validList);
// }
// return null;
// }
}
