import {Component} from '@angular/core';
import {DynamicField} from './dynamic-form/components/dynamic-field.interface';
import {Validators} from '@angular/forms';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	config: DynamicField[] = [
		{
			type: 'input',
			name: 'nome',
			label: 'Inset Name',
			placeholder: '...',
			key: 'name',
			validations: [
				{
					name: 'required',
					validator: Validators.required,
					message: 'Name Mandatory'
				},
				{
					name: 'pattern',
					validator: Validators.pattern('^[a-zA-Z]+$'),
					message: 'Do not try to fool me'
				}
			]
		},
		{
			type: 'select',
			name: 'profession',
			label: 'Current Profession',
			options: [
				'Hacker',
				'HR',
				'Coffee-Bringer'
			],
			placeholder: 'Select...',
			key: 'prof'
		},
		{
			type: 'radio',
			name: 'gender',
			options: [
				'Male',
				'Female'
			],
			key: 'gender',
			validations: [
				{
					name: 'required',
					validator: Validators.required,
					message: 'Gender is Mandatory'
				}
			]
		},
		{
			type: 'checkbox',
			name:'terms',
			label: 'I accept terms and conditions',
			placeholder: 'I accept terms and conditions',
			key: 'terms'
		}
	];

}
